import { getPokemon } from "@/services/pokemon.service"
import { useEffect, useState } from "react"

export const usePokemon = () => {
    const [pokemon,setPokemon] = useState<any>()
    const [loading,setLoading] = useState<boolean>(true)

    const fetchData = async () => {
        try{
            const responsePokemon = await getPokemon()
            setPokemon(responsePokemon)
            setLoading(false)
        }catch(e){
            console.log(e)
            setLoading(false)
        }
        
    }
    useEffect(() => {
        fetchData()
    },[])

    return {pokemon,loading}
}
