import axios from "axios";

export const instancePokemon = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_POKEMON,
    timeout: 5000,
    headers: { 'X-Custom-Header': 'foobar' }
});

instancePokemon.interceptors.request.use(function (config) {    
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});
