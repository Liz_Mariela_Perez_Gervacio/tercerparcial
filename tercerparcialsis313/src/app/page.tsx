import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import { Grid } from '@mui/material';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import Categoryy from '@/componentes/Categoryy';
import Sportse from '@/componentes/Sportse';


export default function ButtonAppBar() {
  return (
    <Box>
      <Box sx={{ display: 'flex' }}>
        <AppBar position="static" sx={{ backgroundColor: "transparent" }}>
          <Toolbar>
            <Typography variant="h6" component="div" sx={{ display: "flex" }} >
              <img src="/imagenes/icon.svg" alt="Italian Trulli"></img>
              <Grid sx={{ justifyContent: "space-between" }}>
                <ul>
                  <li>Home</li>
                  <li>Category</li>
                  <li>Trending News</li>
                  <li>Recent News</li>
                  <li>Clubs Ranking</li>
                  <li>Sports Article</li>
                </ul>
              </Grid>
            </Typography>
            <Button sx={{ color: "#FFFFFF", fontSize: "15px", fontWeight: "500", backgroundColor: "#B8C2CE", borderRadius: "5px", margin: "5px 5px", marginLeft: "100px" }}> Search</Button>
          </Toolbar>
        </AppBar>
      </Box>
      <Grid sx={{ display: "flex" }}>
        <Grid sx={{ width: " 37%", marginTop: "25px" }}>
          <Typography sx={{ fontSize: "65px", fontWeight: "800", marginTop: "20%", lineHeight: "100%" }}>
            TOP SOCERO TO THE FINAL MATCH
          </Typography>
        </Grid>
        <Grid sx={{ width: " 35%", color: "#262626", marginTop: "38%" }}>
          <Typography variant="h6" gutterBottom>
            The EuroLeague Finals Top Scorer is the individual award for the player that gained the highest points in the EuroLeague Finals
          </Typography>
          <Grid>
            <Button sx={{ color: "#E1E8F0", backgroundColor: "#262626", letterSpacing: " 0.20em", borderRadius: "5px", marginTop: "3%", fontSize: "130%" }}>continue reading</Button>
          </Grid>
        </Grid>
        <Grid sx={{ width: " 30%", size: "50px" }}>
          <Grid sx={{ width: "100%", height: "20%", marginTop: "10%" }}>
            <Card sx={{ maxWidth: 345 }}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="140"
                  image="imagenes/img1.svg"
                  alt="green iguana"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="div" color="#696868">
                    Race98 - 03 June 2023
                  </Typography>
                  <Typography variant="h5" color="#262626" >
                    Ethiopian runners took the top four spots.
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid >
          <Grid sx={{ width: " 110%", marginTop: "45%" }}>
            <Card sx={{ maxWidth: 345 }}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="140"
                  image="/imagenes/img2.svg"
                  alt="green iguana"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="div" color="#696868">
                    INDYCAR - 03 June 2023
                  </Typography>
                  <Typography variant="h5" color="#262626" fontWeight="600">
                    IndyCar Detroit: Dixon quickest in second practice
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        </Grid>
      </Grid>
      <Typography gutterBottom variant="h6" color="#262626" fontSize="25px" fontWeight="450">
        Category
      </Typography>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>

        <Categoryy name="Football" any="imagenes/img3.svg"></Categoryy>
        <Categoryy any="imagenes/img3.svg" name="bascket ball" ></Categoryy>
        <Categoryy name="car sport" any="imagenes/img3.svg"></Categoryy>
        <Categoryy name="Table Tennis" any="imagenes/img3.svg"></Categoryy>
      </Box>
      <Box>
      </Box>
      <Typography gutterBottom variant="h6" color="#262626" fontSize="25px" fontWeight="450">
        Sports Article
      </Typography>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Sportse musi="/imagenes/img4.svg" name="Jake Will." pos="5 Exercises Basketball Players Should Be Using To Develop Strength" parafo="This article was written by Jake Willhoite from Healthlisted.com Strength in basketball isn’t all about a massive body mass or ripped muscles."></Sportse>
        <Sportse musi="/imagenes/img4.svg" name="Jake Will." pos="5 Exercises Basketball Players Should Be Using To Develop Strength" parafo="This article was written by Jake Willhoite from Healthlisted.com Strength in basketball isn’t all about a massive body mass or ripped muscles."></Sportse>
        <Sportse musi="/imagenes/img4.svg" name="Jake Will." pos="5 Exercises Basketball Players Should Be Using To Develop Strength" parafo="This article was written by Jake Willhoite from Healthlisted.com Strength in basketball isn’t all about a massive body mass or ripped muscles."></Sportse>
      </Box>
    </Box>

  );
}
