import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';


type Sportse = {
  musi: any,
  name?: any,
  pos:any,
  parafo:any,
}
const inco = (props: Sportse) => {
  const { name, musi ,pos,parafo} = props;
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardMedia
        component="img"
        height="194"
        image={musi}
        alt="Paella dish"
      />

      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
            R
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={name}

      />
      <Typography variant="h7" color="#262626" fontWeight= "700">
        {pos}
      </Typography>
      <Typography variant="body2" color="#696868" marginTop="10%" >
       {parafo}
      </Typography>
      <CardContent>

      </CardContent>
      <CardActions disableSpacing>


      </CardActions>

    </Card>
  );
}

export default inco