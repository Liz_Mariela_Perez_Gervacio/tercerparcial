import { instancePokemon } from "@/configuration/config-pokemon"

export const getPokemon = async () => {
    //https://pokeapi.co/api/v2/pokemon?limit=5&offset=0
    const response = await instancePokemon.get('/pokemon?limit=4&offset=0')    
    return response.data
}
